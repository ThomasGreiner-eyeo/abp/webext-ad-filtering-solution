## Environment

<!-- Please provide the following: -->

* Browser and full version:
* Extension using this toolkit: <!-- example test-mv2 -->
* Commit hash:
* Operating system:

<!-- Note: you can get the commit hash by running: `git log --pretty=format:'%h' -n 1`. -->

## Steps to reproduce

1. (e.g. Load the test extension)
2. (e.g. Run the following code in the background page console)

## Actual behavior

<!-- What behavior do you experience when following the above steps? -->

## Expected behavior

<!-- What behavior do you expect to see instead? -->

## Analysis

<!-- The following section is for engineering and can be skipped when
reporting a bug. -->

### Root cause

<!-- Describe why the bug. -->

### Documentation updates

<!-- Is any documentation change needed? -->

## Acceptance checklist

<!-- This lists the conditions to consider the issue resolved.
This list can be completed during the process of fixing the bug.
-->

Please make sure the following items are complete prior to resolving
the issue:
- [ ] Environment.
- [ ] Reproduction steps.
- [ ] Root cause of the bug.
- [ ] Documentation updates.
- [ ] Release notes added

/label ~bug
