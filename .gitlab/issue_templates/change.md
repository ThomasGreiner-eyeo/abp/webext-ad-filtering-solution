## What does the change do?

<!-- Describe what it does, and possibly give some background. -->

### Use case

<!-- Explain why it is needed. -->

## Error conditions

<!-- As the feature is refined, clarify the error handling scenarios. -->

## How to test

<!-- Please clarify the testing scenario, possibly as it is implemented. -->

## What to change

<!-- What needs to be done to implement this change? This can be filled
as the implementation goes. -->

## Documentation changes

<!-- The documentation changes. -->

## Acceptance checklist

<!-- This lists the conditions to consider the issue resolved.
This list can be completed during the process of fixing the bug.
-->

Please make sure the following items are complete prior to resolving
the issue:
- [ ] What does it do?
- [ ] Error conditions and handling.
- [ ] Testing scenario.
- [ ] Documenation changes.
- [ ] Release notes added.
